import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

class FruitRageMain
{
	public static void main (String args[]) throws IOException
	{
		int set_depth = 4;
		FruitRage h2 = new FruitRage();
		InputData id = h2.readInput("input.txt");
		Answer ans = h2.minMaxAlphaBeta(id.matrix, 0, set_depth, id.time);
		h2.writeToFile(ans, "output.txt");
	}

}

class FruitRage {
	
	int function_count = 0;
    int prune_count = 0;
    
    public void findAllConnected(int x, int y, char matrix[][], List<Integer> cl, char val)
    {
    	int n = matrix.length;
    	if(x<0 || y<0 || x>=n || y>=n || matrix[x][y]!=val)
    		return;
    	matrix[x][y] = '*';
    	cl.add(x*n+y);
    	
    	findAllConnected(x+1,y, matrix, cl, val);
    	findAllConnected(x-1,y, matrix, cl, val);
    	findAllConnected(x,y+1, matrix, cl, val);
    	findAllConnected(x,y-1, matrix, cl, val);
    	
    }
 
	public char[][] copy_matrix(char matrix[][])
	{
		char newmat[][] = new char[matrix.length][matrix.length];
		for (int i=0; i< matrix.length; i++)
		{
			for (int j=0; j< matrix.length; j++)
			{
				newmat[i][j] = matrix[i][j];
			}
		}
		
		return newmat;
	}
	
public char[][] create_new_state(List<Integer> positions, char[][] matrix)
{
    char[][] mat = copy_matrix(matrix);
    int n = mat.length;
    for (int position : positions)
    {
        int row = (position / n);
        int col = position % n;
        mat[row][col] = '*';
    }
    for (int j=0; j<mat.length; j++)
    {
        int count_star = 0;
        for (int i=mat.length-1;i>=0;i--)
        {
            if (mat[i][j] == '*')
            	count_star++;
            	else if(mat[i][j] != '*' && count_star != 0)
            	{
            		mat[i + count_star][j] = mat[i][j];
            		mat[i][j] = '*';
            	}
            }
        }
    return mat;
 }

public Answer minMaxAlphaBeta(char[][] matrix1, int max_depth, int set_depth, double time) throws IOException
{
    char[][] matrix_copy = copy_matrix((matrix1));
    function_count+=1;
    String optimal_move = "";
    int alpha = Integer.MIN_VALUE;
    int beta = Integer.MAX_VALUE;
    int score_to_be_returned = -1;
    char[][] optimal_state = new char[matrix1.length][matrix1.length];
    Comparator<PQType> cmp = new PQComp();
    PriorityQueue<PQType> pq = new PriorityQueue<>(cmp);
    
    for (int i=0; i<matrix_copy.length; i++)
        for (int j=0; j<matrix_copy.length; j++)
        {
            if (matrix_copy[i][j]!='*')
            {
                List<Integer> res = new ArrayList<>();
                findAllConnected(i,j, matrix_copy, res, matrix_copy[i][j]);
                PQType node = new PQType(i*matrix1.length + j, res.size()*res.size(), res);
                pq.add(node);
            }
        }
    int branching_factor = pq.size();
    int n = matrix1.length;
    
    /* Deciding the depth */
    if (branching_factor <= ((n*n)/2))
    {
    	if(n<=5)
    	{
    		if (time < 2)
    		{
    			set_depth = 1;
    		}
    		else
    			set_depth=5;
    	}
    	else if(n>5 && n<=10)
    	{
    		if (time <=8)
    		{
    			set_depth=3;
    		}
    		else
    		{
    			set_depth=4;
    		}
    	}
    	else if(n>10 && n<=15)
    	{
    		if (time<8)
    		{
    			set_depth=3;
    		}
    		else
    		{
    			set_depth=4;
    		}
    	}
    	else if(n>16 && n<=20)
    	{
    		if (time<10)
    		{
    		 set_depth = 3;
    		}
    		else
    		{
    			set_depth = 4;
    		}
    	}
    	else
    	{
    		if (time <=15)
    			set_depth = 3;
    		else
    			set_depth = 4;
    	}
    }
    else
    {
    	if (n>5 && n<=10)
    	{
    		if (time<=3)
    			set_depth = 2;
    		else
    			set_depth = 3;
    	}
    	
    	else if (n<=15)
    	{
    		set_depth = 2;
    	}
    	
    	else
    	{
    		set_depth = 2;
    	}
    	
    }
    
    boolean flag = false;
    while(!pq.isEmpty())
    {
    	PQType elem = pq.remove();
    	char[][] new_state = create_new_state(elem.ls, matrix1);
    	if(flag==false)
    	{
    		writeToFile(new Answer(convertRowCol(elem.pos, n), new_state, 0), "output.txt");
    		flag = true;
    	}
        int score = elem.score;
        int score_copy = score;
        char new_state_copy[][] = copy_matrix(new_state);
        int utility = minFn(new_state, alpha, beta, score, max_depth + 1, set_depth);
        if (utility > alpha)
        {
            alpha = utility;
            optimal_move = convertRowCol(elem.pos, matrix1.length);
            optimal_state = copy_matrix(new_state_copy);
            score_to_be_returned = score_copy;
        }
    }
    System.out.println("function call count "+function_count);
    System.out.println("Prune count "+prune_count);
	return new Answer(optimal_move, optimal_state, score_to_be_returned);

}

public int minFn(char[][] matrix, int alpha, int beta, int score, int max_depth, int set_depth)
{
    function_count += 1;
    char[][] matrix_copy = copy_matrix(matrix);
    if ( max_depth >= set_depth || checkTerminalCondition(matrix))
    {
        return score;
    }
    
    int v = Integer.MAX_VALUE;
    Comparator<PQType> cmp = new PQComp();
    PriorityQueue<PQType> pq2 = new PriorityQueue<>(cmp);
    
    for (int i=0; i<matrix_copy.length; i++)
    {
        for (int j=0; j<matrix_copy.length; j++)
        {
            if (matrix_copy[i][j]!='*')
            {
                List<Integer> res = new ArrayList<>();
                findAllConnected(i,j, matrix_copy, res, matrix_copy[i][j]);
                PQType node = new PQType(i*matrix.length + j, res.size()*res.size(), res);
                pq2.add(node);
            }
        }
    }

    while(!pq2.isEmpty())
    {
    	PQType elem = pq2.remove();
    	int incoming_score = score;
        char new_state[][] = create_new_state(elem.ls, matrix);
        incoming_score -= elem.score;
        v = Math.min(v, maxFn(new_state, alpha, beta, incoming_score, max_depth+1, set_depth));
        if (v <= alpha)
        {
            prune_count+=1;
            return v;
        }
        beta = Math.min(beta, v);
    	    
    }
    return v;

}

public int maxFn(char[][] matrix, int alpha, int beta, int score, int max_depth, int set_depth)
{
    function_count += 1;
    char[][] matrix_copy = copy_matrix(matrix);
    if ( max_depth >= set_depth || checkTerminalCondition(matrix))
    {
        return score;
    }
    
    int v = Integer.MIN_VALUE;
    Comparator<PQType> cmp = new PQComp();
    PriorityQueue<PQType> pq2 = new PriorityQueue<>(cmp);
    
    for (int i=0; i<matrix_copy.length; i++)
    {
        for (int j=0; j<matrix_copy.length; j++)
        {
            if (matrix_copy[i][j]!='*')
            {
                List<Integer> res = new ArrayList<>();
                findAllConnected(i,j, matrix_copy, res, matrix_copy[i][j]);
                PQType node = new PQType(i*matrix.length + j, res.size()*res.size(), res);
                pq2.add(node);
            }
        }
    }

    while(!pq2.isEmpty())
    {
    	PQType elem = pq2.remove();
    	int incoming_score = score;
        char new_state[][] = create_new_state(elem.ls, matrix);
        incoming_score += elem.score;
        v = Math.max(v, minFn(new_state, alpha, beta, incoming_score, max_depth+1, set_depth));
        if (v >= beta)
        {
            prune_count+=1;
            return v;
        }
        alpha = Math.max(alpha, v);
    	    
    }
    return v;

}


public boolean checkTerminalCondition(char[][] matrix)
{
    for(int e=0; e<matrix.length; e++)
    {
        for (int f=0; f<matrix.length; f++)
        {
        	if (matrix[e][f] != '*')
                return false;
        }
    }
            
    return true;
}

public String convertRowCol(int val, int n)
{
	Map<Integer, String> col_conversion = new HashMap<>();
	col_conversion.put(1, "A");col_conversion.put(2, "B");col_conversion.put(3, "C");
	col_conversion.put(4, "D");col_conversion.put(5, "E");col_conversion.put(6, "F");
	col_conversion.put(7, "G");col_conversion.put(8, "H");col_conversion.put(9, "I");
	col_conversion.put(10, "J");col_conversion.put(11, "K");col_conversion.put(12, "L");
	col_conversion.put(13, "M");col_conversion.put(14, "N");col_conversion.put(15, "O");
	col_conversion.put(16, "P");col_conversion.put(17, "Q");col_conversion.put(18, "R");
	col_conversion.put(19, "S");col_conversion.put(20, "T");col_conversion.put(21, "U");
	col_conversion.put(22, "V");col_conversion.put(23, "W");col_conversion.put(24, "X");
	col_conversion.put(25, "Y");col_conversion.put(26, "Z");
    int row = (val/n) + 1;
    int col = val%n + 1;
    String converted_pos = col_conversion.get(col) + row;
    return converted_pos;
}

public InputData readInput(String filename)
{
	char input_matrix [][] = null;
	InputData input_obj = null;
	int line_no = 0;
	int n = 0;
	int p = 0;
	double time = 0.0;
	int row_no = 0;
	boolean matrix_initialization_flag = false;
	String current = "";
	try 
	{
		BufferedReader br = new BufferedReader(new FileReader(filename));
		while ((current = br.readLine()) != null) 
		{
			if (line_no == 0)
			{
				n = Integer.parseInt(current);
			}
			else if (line_no == 1)
			{
				p = Integer.parseInt(current);
			}
			else if (line_no == 2)
			{
				time = Double.parseDouble(current);
			}
			else if (matrix_initialization_flag == false)
			{
				matrix_initialization_flag = true;
				input_matrix = new char[n][n];
				for (int i=0; i<n; i++)
				{
					input_matrix[row_no][i] = current.charAt(i);
				}
				row_no++;
			}
			else
			{
				for (int j=0; j<n; j++)
				{
					input_matrix[row_no][j] = current.charAt(j);
				}
				row_no++;
			}
			line_no++;		
		}
		br.close();
		input_obj = new InputData(n, p, time, input_matrix);
	} 
	catch (IOException e) 
	{
		e.printStackTrace();
	}
	
	return input_obj;
}

public void writeToFile(Answer ans, String filename) throws IOException
{
	BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
	bw.write(ans.position+"\n");
	for (int i=0; i<ans.optimal_state.length; i++)
	{
		for (int j=0; j<ans.optimal_state.length; j++)
		{
			bw.write(ans.optimal_state[i][j]);
		}
		bw.write("\n");
	}
	bw.close();
}

}

class Answer
{
	String position;
	char[][] optimal_state;
	int score;
	
	public Answer(String position, char[][] optimal_state, int score)
	{
		this.position = position;
		this.optimal_state = optimal_state;
		this.score = score;
	}
}

class PQType
{
	int pos;
	int score;
	List<Integer> ls = new ArrayList<>();
	
	public PQType(int pos, int score, List<Integer> positions)
	{
		this.pos = pos;
		this.score = score;
		this.ls = positions; 
	}
}

class PQComp implements Comparator<PQType>
{

	@Override
	public int compare(PQType arg0, PQType arg1) {
		// TODO Auto-generated method stub
		if (arg0.score < arg1.score)
			return 1;
		else if (arg0.score > arg1.score)
			return -1;
		else
			return 0;
}
}

class InputData
{
	int n;
	int p;
	double time;
	char matrix[][] = new char[n][n];
	
	public InputData(int n, int p, double time, char matrix[][])
	{
		this.n = n;
		this.p = p;
		this.time = time;
		this.matrix = matrix;
	}
}