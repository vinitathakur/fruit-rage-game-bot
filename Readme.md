# Fruit Rage Game Bot

- Fruit rage is a two-player game in which each player tries to maximize his/her share from a 2-D grid of fruits in which several fruits are connected to each other vertically or horizontally
- Returns the next best move to be taken by a player, given the current game state using Min-Max  algorithm with alpha-beta pruning

### Input and Output
* The input is a text file called ```input.txt```. The first line is the size of the n * n square board. The second line is the number of fruit types. The third line is the remaining time in seconds. The next n lines denote the board. Each character can be either a digit from	0 to p-1, or a * to	denote an empty	cell
* The output file is called ```output.txt```. The first line contains the selected move as two characters
* A	letter from	A to Z representing	the	column number (where A is the leftmost column,	B is the next one to the right,	etc), and A	number from	1 to 26	representing the row	number (where 1	is the top row,	2 is the row below it, etc)
* Refer to the sample input and output files

### How to Run ?
* Complile using ```javac FruitRageMain.java```. Run using ```java FruitRageMain```
* The program produces an ```output.txt``` file in the same directory from which it is run